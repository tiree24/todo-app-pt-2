import React, { Component } from "react";
import todosList from "./todos.json";
import TodoItem from "./components/TodoItem";
import TodoList from "./components/TodoList";
import { Router, Switch } from "react-router-dom";
import Navigation from "./components/Navigation";

class App extends Component {
  state = {
    todos: todosList,
  };

  addToDoList = (event) => {
    if (event.key === "Enter") {
      const newToDo = {
        userId: 1,
        id: Math.floor(Math.random() * 10000),
        title: event.target.value,
        completed: false,
      };

      const newToDos = this.state.todos.slice();
      newToDos.push(newToDo);
      this.setState({ todos: newToDos });
      event.target.value = "";
    }
  };

  handleComplete = (event, id) => {
    let newToDos = this.state.todos.map((todo) => {
      if (todo.id === id) {
        return {
          ...todo,
          completed: !todo.completed,
        };
      }
      return todo;
    });
    this.setState({
      todos: newToDos,
    });
  };

  handleDelete = (event, id) => {
    let newToDos = this.state.todos.filter((todo) => todo.id !== id);
    this.setState({ todos: newToDos });
  };

  clearComplete = (event) => {
    let newToDos = this.state.todos.filter((todo) => todo.complete === false);
    this.setState({ todos: newToDos });
  };

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input className="new-todo" placeholder="What needs to be done?" onKeyDown={this.addToDoList} autoFocus />
        </header>
        <TodoList todos={this.state.todos} handleDelete={this.handleDelete} handleComplete={this.handleComplete} />

        <Navigation />
      </section>
    );
  }
}

export default App;
