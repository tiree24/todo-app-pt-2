import React from "react";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import App from "../App";
class Navigation extends React.Component {
  render() {
    return (
      <footer className="footer">
        {/* <!-- This should be `0 items left` by default --> */}
        <span className="todo-count">
          <strong>0</strong> item(s) left
        </span>

        <Router>
          <ul className="filters">
            <li>
              <Link to="/">
                <a href="/all">All</a>
              </Link>
            </li>
            <li>
              <Link to="/Active">
                <a href="/active">Active</a>
              </Link>
            </li>
            <li>
              <Link to="/Completed">
                <a href="/completed">Completed</a>
              </Link>
              <button onClick={this.clearCompleted} className="clear-completed">
                Clear completed
              </button>
            </li>
          </ul>

          <Switch>
            <Route path="/all">
              <All />
            </Route>
            <Route path="/active">
              <Active />
            </Route>
            <Route path="/completed">
              <Completed />
            </Route>
          </Switch>
        </Router>
      </footer>
    );
  }
}

function All() {
  return <h2></h2>;
}

function Active() {
  return <h2></h2>;
}

function Completed() {
  return <h2></h2>;
}

function clearComplete() {
  return <h2></h2>;
}

export default Navigation;
