import React from "react";
import TodoItem from "./TodoItem";

class TodoList extends React.Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              title={todo.title}
              handleDelete={(event) => {
                this.props.handleDelete(event, todo.id);
              }}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default TodoList;
